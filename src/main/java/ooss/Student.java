package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        String introduce = String.format("My name is %s. I am %d years old. I am a student.", this.name, this.age);
        if (this.klass != null) {
            if (klass.getLeader().getId() == this.id) {
                introduce += String.format(" I am the leader of class %d.", this.klass.getNumber());
            } else {
                introduce += String.format(" I am in class %d.", this.klass.getNumber());
            }
        }
        return introduce;
    }

    public Klass getKlass() {
        return klass;
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public Boolean isIn(Klass klass) {
        if (this.klass == null) return false;
        return klass.getNumber() == this.klass.getNumber();
    }

    @Override
    public void say(Student leader) {
        System.out.println( String.format("I am %s, student of Class %d. I know %s become Leader.",
                this.name,this.getKlass().getNumber(), leader.getName()));
    }
}
