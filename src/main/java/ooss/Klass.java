package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    List<Person> attachPersons;
    private Student leader;

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }


    public Klass(int number) {
        this.number = number;
        attachPersons = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student king) {
        if (king.getKlass() == null || king.getKlass().getNumber() != this.number) {
            System.out.println("It is not one of us.");
        } else {
            this.leader = king;
            attachPersons.forEach(person -> person.say(king));
        }
    }

    public boolean isLeader(Student tom) {
        return this.leader.equals(tom);
    }

    public void attach(Person person) {
        attachPersons.add(person);
    }
}
