package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }

    @Override
    public String introduce() {
        String classIntroduce = klasses.size() == 0 ? "" : String.format(" I teach Class %s.",
                klasses.stream().map(klass -> klass.getNumber()).map(String::valueOf).collect(Collectors.joining(", ")));
        return String.format("My name is %s. I am %d years old. I am a teacher.%s", this.name, this.age,classIntroduce);
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }


    public boolean belongsTo(Klass klass) {
        return klasses.stream().anyMatch(klass1 -> klass1.equals(klass));
    }

    public boolean isTeaching(Student tom) {
        return klasses.stream().anyMatch(klass1 -> klass1.equals(tom.getKlass()));
    }

    @Override
    public void say(Student leader) {
        System.out.println( String.format("I am %s, teacher of Class %s. I know %s become Leader.",
                this.name,
                klasses.stream().map(klass -> klass.getNumber()).map(String::valueOf).collect(Collectors.joining(", "))
                , leader.getName()));
    }
}
